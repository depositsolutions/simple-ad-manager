'''
Module to simplify the usage of the LDAP3 package to manage an Active Directory.
'''
from __future__ import absolute_import

import logging

import ldap3
from ldap3 import ALL_ATTRIBUTES, LEVEL, SUBTREE
from ldap3 import MODIFY_ADD, MODIFY_DELETE, MODIFY_REPLACE
from ldap3.extend.microsoft.addMembersToGroups import ad_add_members_to_groups
from ldap3.extend.microsoft.removeMembersFromGroups import ad_remove_members_from_groups

import re

from . import err

log = logging.getLogger(__name__)


class DomainController:
    '''
    Main object to establish and use any given LDAP connection.
    '''

    def __init__(self, cfg, ldap_connection):
        self.ldap_host = cfg['ldap-host']
        self.ldap_base = cfg['ldap-base']
        self.ldap_con = ldap_connection

    def _find_ad_entry(self, object_name):
        '''
        Helper method to lookup an AD entry if no DN is given.

        Returns:
            CN=Group or Username,OU=some.ou,DC=example,DC=com
        '''
        ldap_query = '(|(samaccountname={obj})(cn={obj}))'.format(obj=object_name)
        self.ldap_con.search(self.ldap_base, ldap_query, attributes=ALL_ATTRIBUTES)

        try:
            full_dn = self.ldap_con.entries[0].distinguishedName.value
        except IndexError:
            msg = '{obj}'.format(obj=object_name)
            log.error(msg)
            raise err.LdapSearchError

        return full_dn

    def _create_query_dn(self, obj):
        '''
        Formats a sAMAccoutName or CN as a DN.
        '''
        if '=' in obj:
            query_object = obj
            if self.ldap_base not in obj:
                msg = 'Either provide distinguished name including base DN or just the CN'
                log.error(msg)
                raise err.LdapSearchError(msg)
        else:
            query_object = self._find_ad_entry(obj)

        return query_object

    def list_object(self, args):
        '''
        Returns a list of given object types from the given search base.

        Arguments as dict:
            type: objectType
            searchbase: tree_down,tree_up,tree_top or OU=tree_down,OU=tree_up,OU_tree_top
        '''

        object_type = args['type']

        # map abreviations to actual AD attributes for simple CLI use
        if args['type'] == 'ou':
            object_type = 'organizationalUnit'
        if args['type'] == 'all':
            object_type = '*'

        return_value = 'cn'
        if args['fulldn']:
            return_value = 'distinguishedName'

        # set the search base
        ldap_rebase = ''
        expected_base_pattern = re.compile(r'(DC|OU)=[a-z\.\-\_0-9]+', re.IGNORECASE)
        # use ldap base from config when nothin else is given
        if args['searchbase'] is None:
            ldap_rebase = self.ldap_base
        # create a distinguishedName in case only a list is given
        elif not expected_base_pattern.match(args['searchbase']):
            ou_tree = args['searchbase'].split(',')
            for level in ou_tree:
                ldap_rebase = ldap_rebase + 'OU={level},'.format(level=level)
            ldap_rebase = ldap_rebase + self.ldap_base
        # add the ldap base if missing in given search base
        elif self.ldap_base not in args['searchbase']:
            ldap_rebase = args['searchbase'] + ',' + self.ldap_base
        else:
            ldap_rebase = args['searchbase']

        log.debug('Searching in {base}'.format(base=ldap_rebase))
        ldap_query = '(objectClass={otype})'.format(otype=object_type)

        # create ldap query
        if args.get('recursive'):
            self.ldap_con.search(ldap_rebase, ldap_query, attributes=ALL_ATTRIBUTES, search_scope=SUBTREE)
        else:
            self.ldap_con.search(ldap_rebase, ldap_query, attributes=ALL_ATTRIBUTES, search_scope=LEVEL)

        # create list of results
        entry_list = []
        for entry in self.ldap_con.entries:
            try:
                # do not append the search base itself
                if entry.distinguishedName.value != ldap_rebase:
                    # return name attribute for organizational units or fulldn if set
                    if 'organizationalUnit' in entry.objectClass.value:
                        if args['fulldn']:
                            entry_list.append(entry[return_value].value)
                        else:
                            entry_list.append(entry.name.value)
                    else:
                        entry_list.append(entry[return_value].value)
            except ldap3.core.exceptions.LDAPCursorError:
                log.debug('{result} did not return values'.format(result=entry))

        if not entry_list:
            log.info('No entries found for {base}'.format(base=ldap_rebase))

        return entry_list

    def create_object(self, args):
        '''
        Creates AD objects in given OU.

        Arguments as dict:
            type: objectType
            name: distinguishedName
            description: Purpose of object

            For group object type:
                domain: local or global
        '''
        # only allow full distinguished names for object creation
        if self.ldap_base not in args['name']:
            msg = 'Please provide distinguished name including base DN'
            log.error(msg)
            raise err.LdapError(msg)
        else:
            object_dn = args['name']

        object_type = args['type']
        object_attributes = {}

        # map abreviations and flags to AD specific values
        if object_type == 'ou':
            object_type = 'organizationalUnit'
        if object_type == 'group':
            if args['domain'] == 'global':
                group_type = '-2147483646'
            if args['domain'] == 'local':
                group_type = '-2147483644'
            object_attributes['groupType'] = group_type

        # add a description to the created object if set
        if args.get('description'):
            object_attributes['description'] = args.get('description')

        # create the object and check the result
        object_class = ['top', object_type]
        self.ldap_con.add(object_dn, object_class, object_attributes)

        if self.ldap_con.result['result'] != 0:
            msg = ('Object creation failed with message: {message}'.format(message=self.ldap_con.result['message']))
            log.error(msg)
            raise err.LdapProcessError(msg)
        else:
            log.info('{obj} sucessfully created.'.format(obj=args['name']))

    def remove_object(self, args):
        '''
        Removes a given object from the Active Directory.

        Arguments as dict:
            'name': 'full distinguishedName or CN'
        '''

        object_name = self._create_query_dn(args.get('name'))

        try:
            self.ldap_con.delete(object_name)
            log.info('Removed {obj}'.format(obj=object_name))
        except Exception:
            msg = 'Could not remove {obj}'.format(obj=object_name)
            log.error(msg)
            raise

    def get_attribute(self, args):
        '''
        Returns list of values for given attributes
        '''
        attribute_list = []

        log.debug('Showing {attr} of {obj} on {host}'
                  .format(attr=args['attribute'], obj=args['name'], host=self.ldap_host))

        # create default ldap query
        return_attribute = args['attribute']

        ldap_query = (
            '(distinguishedName={obj})'
            .format(obj=self._create_query_dn(args['name']))
        )

        # create ldap queries for member and memberof attributes
        # map recursive flag for member attribute to AD specific number code
        if args['attribute'] == 'member':
            if args['recursive'] is True:
                ldap_query = (
                    '(&(objectClass=*)(memberOf:1.2.840.113556.1.4.1941:={grp}))'
                    .format(grp=self._create_query_dn(args['name']))
                )
            if args['recursive'] is False:
                ldap_query = (
                    '(&(objectClass=*)(memberOf={grp}))'
                    .format(grp=self._create_query_dn(args['name']))
                )
            return_attribute = 'cn'

        if args['attribute'] == 'memberof':
            ldap_query = (
                '(&(objectClass=*)(member={obj}))'
                .format(obj=self._create_query_dn(args['name']))
            )
            return_attribute = 'cn'

        # run the query and create result list
        self.ldap_con.search(self.ldap_base, ldap_query, attributes=ALL_ATTRIBUTES)

        try:
            for entry in self.ldap_con.entries:
                attribute_list.append(entry[return_attribute].value)
        except ldap3.core.exceptions.LDAPKeyError:
            msg = 'No attribute called "{attr}" found for {obj}'.format(attr=return_attribute, obj=args['name'])
            log.error(msg)
            raise err.LdapSearchError

        if not attribute_list:
            log.info('No entries found for {obj}'.format(obj=args['name']))

        return attribute_list

    def update_object(self, args):
        '''
        Updates given parameter of given object.

        Arguments as dict:
            name: distinguishedName
            task: add, remove, update
            attribute: AD object attribute (member, memberof, description)
            value: value to set attribute to
        '''
        # avoid total confusion - only allow updating members, not memberofs
        if args['attribute'] == 'memberof':
            msg = 'Adding memberof not supported, please refer to member attribute.'
            log.error(msg)
            raise err.UnsupportedArgument

        # only allow adding and removing if updated attribute is member
        if args['attribute'] == 'member':
            if args['task'] not in ('add', 'remove'):
                msg = '{arg} not supported for member attribute.'.format(arg=args['task'])
                log.error(msg)
                raise err.UnsupportedArgument

            log.debug('Adding {obj} to {grp}'.format(obj=args['value'], grp=args['name']))
            username = self._create_query_dn(args['value'])
            groupname = self._create_query_dn(args['name'])

            if args['task'] == 'add':
                try:
                    ad_add_members_to_groups(self.ldap_con, [username], [groupname], fix=True)
                    log.info('Added {obj} to {grp}'.format(obj=args['value'], grp=args['name']))
                except Exception:
                    msg = 'Adding {obj} to {grp} failed'.format(obj=args['value'], grp=args['name'])
                    log.error(msg)
                    raise err.LdapProcessError(msg)
            if args['task'] == 'remove':
                try:
                    ad_remove_members_from_groups(self.ldap_con, [username], [groupname], fix=True)
                    log.info('Removed {obj} from {grp}'.format(obj=args['value'], grp=args['name']))
                except Exception:
                    msg = 'Removing {obj} from {grp} failed'.format(obj=args['value'], grp=args['name'])
                    log.error(msg)
                    raise err.LdapProcessError(msg)

        # update whatever is given as long as it is not the member attribute(s)
        else:
            log.debug('Updating "{o}" attribute "{a}" with "{t}" "{v}"'
                      .format(o=args['name'], a=args['attribute'], t=args['task'], v=args['value']))

            if args['task'] == 'add':
                modify_query = {args['attribute']: [(MODIFY_ADD, [args['value']])]}
            if args['task'] == 'remove':
                if args['value'] == 'all':
                    modify_query = {args['attribute']: [(MODIFY_DELETE, [])]}
                else:
                    modify_query = {args['attribute']: [(MODIFY_DELETE, [args['value']])]}
            if args['task'] == 'update':
                modify_query = {args['attribute']: [(MODIFY_REPLACE, [args['value']])]}

            try:
                self.ldap_con.modify(self._create_query_dn(args['name']), modify_query)
            except Exception:
                msg = 'Failed to {task} {attr}'.format(task=args['task'], attr=args['attribute'])
                log.error(msg)
                raise
