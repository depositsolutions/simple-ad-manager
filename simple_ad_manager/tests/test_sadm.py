from ldap3 import Server, Connection, MOCK_SYNC, OFFLINE_AD_2012_R2, ALL_ATTRIBUTES
import pytest

from simple_ad_manager import sadm
from simple_ad_manager import err


class TestLdap:

    def setup_method(self):
        self.test_cfg = {
            'ldap-host': 'my_fake_server',
            'ldap-base': 'OU=test,O=lab'
        }

        self.test_base = self.test_cfg['ldap-base']
        self.test_host = self.test_cfg['ldap-host']

        self.test_ldap_srv = Server(self.test_host, get_info=OFFLINE_AD_2012_R2)
        self.test_ldap_con = Connection(
            self.test_ldap_srv,
            user='CN=my_user,OU=test,O=lab',
            password='my_password',
            client_strategy=MOCK_SYNC
        )

        self.test_ldap_con.strategy.add_entry(
            'cn=my_user,ou=test,o=lab',
            {
                'userPassword': 'my_password',
                'sn': 'user_sn',
                'revision': 0
            }
        )
        # Test user entry
        self.test_ldap_con.strategy.add_entry(
            'CN=Some User,OU=test,O=lab',
            {
                'objectClass': ['top', 'user'],
                'CN': 'Some User CN',
                'memberOf': ['SOME_GROUP'],
                'distinguishedName': 'Some User',
                'sAMAccountName': 'some.user',
                'description': 'Some description'
            }
        )

        # Test group entry
        self.test_ldap_con.strategy.add_entry(
            'CN=SOME_GROUP,OU=test,O=lab',
            {
                'objectClass': ['top', 'group'],
                'member': ['Some User', 'NESTED_GROUP'],
                'cn': 'SOME_GROUP_CN',
                'distinguishedName': 'SOME_GROUP',
                'description': 'Some description'
            }
        )

        # Test group entry
        self.test_ldap_con.strategy.add_entry(
            'CN=NESTED_GROUP,OU=test,O=lab',
            {
                'objectClass': ['top', 'group'],
                'memberOf': ['SOME_GROUP'],
                'cn': 'NESTED_GROUP_CN',
                'distinguishedName': 'NESTED_GROUP'
            }
        )

        # Test OU entry
        self.test_ldap_con.strategy.add_entry(
            'OU=SOME_OU,OU=test,O=lab',
            {
                'objectClass': ['top', 'organizationalUnit'],
                'name': 'SOME_OU',
                'distinguishedName': 'SOME_OU'
            }
        )

        # Test Deep OU entry
        self.test_ldap_con.strategy.add_entry(
            'OU=SOME_DEEP_OU,OU=SOME_OU,OU=test,O=lab',
            {
                'objectClass': ['top', 'organizationalUnit'],
                'name': 'SOME_DEEP_OU',
                'distinguishedName': 'SOME_DEEP_OU'
            }
        )

        # Test user in OU entry
        self.test_ldap_con.strategy.add_entry(
            'CN=Some Other User,OU=SOME_OU,OU=test,O=lab',
            {
                'objectClass': ['top', 'user'],
                'CN': 'Some Other User CN',
                'memberOf': ['NESTED_GROUP'],
                'distinguishedName': 'Some Other User',
                'sAMAccountName': 'some.other.user'
            }
        )

        # Test user in deep OU entry
        self.test_ldap_con.strategy.add_entry(
            'CN=Some Deep User,OU=SOME_DEEP_OU,OU=SOME_OU,OU=test,O=lab',
            {
                'objectClass': ['top', 'user'],
                'CN': 'Some Deep User CN',
                'distinguishedName': 'Some Deep User',
                'sAMAccountName': 'some.deep.user'
            }
        )

        self.test_ldap_con.bind()
        self.dc = sadm.DomainController(self.test_cfg, self.test_ldap_con)

    def teardown_method(self):
        self.test_ldap_con.unbind()
        pass

    def test_find_ad_entry_no_dn(self):
        testobj = 'SOME_GROUP_CN'
        testentries = self.dc._find_ad_entry(testobj)
        assert testentries == 'SOME_GROUP'

    def test_create_query_dn_with_dn_and_base(self):
        '''
        If a DN is given that includes the base, the returned value needs to be the same.
        '''
        testobj = 'CN=name,' + self.test_base
        assert(self.dc._create_query_dn(testobj) == testobj)

    def test_create_query_dn_with_dn_and_no_base(self):
        '''
        If a DN is given without the base, an exception has to be raised.
        '''
        testobj = 'CN=name,OU=some.ou'
        with pytest.raises(err.LdapSearchError):
            self.dc._create_query_dn(testobj)

    def test_create_query_dn_with_cn(self):
        '''
        If a CN is given it needs to be replaced with the distinguishedName.
        '''
        testobj = 'Some User CN'
        return_value = 'Some User'
        assert(self.dc._create_query_dn(testobj) == return_value)

    def test_create_query_dn_with_samaccountname(self):
        '''
        If a samaccountname is given it needs to be replaced with the DN.
        '''
        testobj = 'some.user'
        return_value = 'Some User'
        assert(self.dc._create_query_dn(testobj) == return_value)

    def test_list_object_no_type_no_base_not_fulldn(self):
        '''
        If base is not further specified, the ldap_base is to be used. None will always be "all" and treated as
        "(objectClass=*)"
        '''
        ou_contents = self.dc.list_object({'type': 'all', 'searchbase': None, 'recursive': True, 'fulldn': False})
        assert 'SOME_GROUP_CN' in ou_contents
        assert 'NESTED_GROUP_CN' in ou_contents
        assert 'Some User CN' in ou_contents
        assert 'Some Other User CN' in ou_contents
        assert 'Some Deep User CN' in ou_contents
        assert 'SOME_OU' in ou_contents
        assert 'SOME_DEEP_OU' in ou_contents

    def test_list_object_no_type_no_base(self):
        '''
        If base is not further specified, the ldap_base is to be used. None will always be "all" and treated as
        "(objectClass=*)"
        '''
        ou_contents = self.dc.list_object({'type': 'all', 'searchbase': None, 'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' in ou_contents
        assert 'NESTED_GROUP' in ou_contents
        assert 'Some User' in ou_contents
        assert 'Some Other User' in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' in ou_contents
        assert 'SOME_DEEP_OU' in ou_contents

    def test_list_object_group_type_no_base(self):
        ou_contents = self.dc.list_object({'type': 'group', 'searchbase': None, 'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' in ou_contents
        assert 'NESTED_GROUP' in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' not in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_user_type_no_base(self):
        ou_contents = self.dc.list_object({'type': 'user', 'searchbase': None, 'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' in ou_contents
        assert 'Some Other User' in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_ou_type_no_base(self):
        ou_contents = self.dc.list_object({'type': 'ou', 'searchbase': None, 'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' not in ou_contents
        assert 'SOME_OU' in ou_contents
        assert 'SOME_DEEP_OU' in ou_contents

    def test_list_object_user_type_with_simple_base(self):
        ou_contents = self.dc.list_object({'type': 'user', 'searchbase': 'SOME_OU', 'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_user_type_with_deep_base(self):
        ou_contents = self.dc.list_object({'type': 'user', 'searchbase': 'SOME_DEEP_OU,SOME_OU', 'recursive': True,
                                           'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_user_type_with_part_base(self):
        ou_contents = self.dc.list_object({'type': 'user', 'searchbase': 'OU=SOME_DEEP_OU,OU=SOME_OU',
                                           'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_user_type_with_full_base(self):
        ou_contents = self.dc.list_object({'type': 'user', 'searchbase': 'OU=SOME_DEEP_OU,OU=SOME_OU,OU=test,O=lab',
                                           'recursive': True, 'fulldn': True})
        assert 'SOME_GROUP' not in ou_contents
        assert 'NESTED_GROUP' not in ou_contents
        assert 'Some User' not in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' in ou_contents
        assert 'SOME_OU' not in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_list_object_all_type_in_base_only(self):
        ou_contents = self.dc.list_object({'type': 'all', 'searchbase': None,
                                           'recursive': False, 'fulldn': True})
        assert 'SOME_GROUP' in ou_contents
        assert 'NESTED_GROUP' in ou_contents
        assert 'Some User' in ou_contents
        assert 'Some Other User' not in ou_contents
        assert 'Some Deep User' not in ou_contents
        assert 'SOME_OU' in ou_contents
        assert 'SOME_DEEP_OU' not in ou_contents

    def test_create_object_no_dn(self):
        with pytest.raises(err.LdapError):
            self.dc.create_object({'type': 'some.object', 'name': 'NOBASE'})

    def test_create_object_user(self):
        self.dc.create_object({'type': 'user', 'name': 'CN=CREATED USER,OU=test,O=lab'})
        test_query = '(&(objectClass=user)(cn=CREATED USER))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'CREATED USER' == self.test_ldap_con.entries[0].cn.value

    def test_create_object_user_with_description(self):
        self.dc.create_object({'type': 'user', 'name': 'CN=CREATED USER,OU=test,O=lab',
                               'description': 'Some description'})
        test_query = '(&(objectClass=user)(cn=CREATED USER))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'Some description' == self.test_ldap_con.entries[0].description.value

    def test_create_object_group_domain_local(self):
        self.dc.create_object({'type': 'group', 'name': 'CN=CREATED_GROUP,OU=test,O=lab', 'domain': 'local'})
        test_query = '(&(objectClass=group)(cn=CREATED_GROUP))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'CREATED_GROUP' == self.test_ldap_con.entries[0].cn.value
        assert -2147483644 == self.test_ldap_con.entries[0].groupType.value

    def test_create_object_group_domain_global(self):
        self.dc.create_object({'type': 'group', 'name': 'CN=CREATED_GROUP,OU=test,O=lab', 'domain': 'global'})
        test_query = '(&(objectClass=group)(cn=CREATED_GROUP))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'CREATED_GROUP' == self.test_ldap_con.entries[0].cn.value
        assert -2147483646 == self.test_ldap_con.entries[0].groupType.value

    def test_create_object_group_with_description(self):
        self.dc.create_object({'type': 'group', 'name': 'CN=CREATED_GROUP,OU=test,O=lab', 'domain': 'local',
                               'description': 'Some group description'})
        test_query = '(&(objectClass=group)(cn=CREATED_GROUP))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'Some group description' == self.test_ldap_con.entries[0].description.value

    def test_create_object_ou(self):
        self.dc.create_object({'type': 'ou', 'name': 'CN=CREATED_OU,OU=test,O=lab', 'domain': 'global'})
        test_query = '(&(objectClass=organizationalUnit)(cn=CREATED_OU))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'CREATED_OU' == self.test_ldap_con.entries[0].cn.value

    def test_create_object_organizationalunit(self):
        self.dc.create_object({'type': 'organizationalUnit', 'name': 'CN=CREATED_OU,OU=test,O=lab', 'domain': 'global'})
        test_query = '(&(objectClass=organizationalUnit)(cn=CREATED_OU))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'CREATED_OU' == self.test_ldap_con.entries[0].cn.value

    def test_create_object_ou_with_description(self):
        self.dc.create_object({'type': 'ou', 'name': 'CN=CREATED_OU,OU=test,O=lab', 'domain': 'global',
                               'description': 'Some OU description'})
        test_query = '(&(objectClass=organizationalUnit)(cn=CREATED_OU))'
        self.test_ldap_con.search(self.test_base, test_query, attributes=ALL_ATTRIBUTES)
        assert 'Some OU description' == self.test_ldap_con.entries[0].description.value

    def test_get_member(self):
        testentries = self.dc.get_attribute({'attribute': 'member', 'name': 'SOME_GROUP', 'recursive': False})
        assert 'Some User CN' in testentries
        assert 'NESTED_GROUP_CN' in testentries
        assert 'Some Other User CN' not in testentries

    def test_get_member_recursive(self):
        '''
        AD specific due to memberOf:1.2.840.113556.1.4.1941. Not mockable.
        '''

    def test_get_memberof(self):
        testentries = self.dc.get_attribute({'attribute': 'memberof', 'name': 'Some User', 'recursive': False})
        assert 'SOME_GROUP_CN' in testentries

    def test_get_description(self):
        testentries = self.dc.get_attribute({'attribute': 'description', 'name': 'Some User'})
        assert 'Some description' in testentries

    def test_get_no_results(self):
        with pytest.raises(sadm.err.LdapSearchError):
            assert self.dc.get_attribute({'attribute': 'description', 'name': 'Some Wrong User'})

    def test_update_memberof(self):
        with pytest.raises(sadm.err.UnsupportedArgument):
            assert self.dc.update_object({'attribute': 'memberof', 'name': 'User', 'value': 'value', 'task': 'add'})

    def test_update_member_not_add_or_remove(self):
        with pytest.raises(sadm.err.UnsupportedArgument):
            assert self.dc.update_object({'attribute': 'member', 'name': 'User', 'value': 'value', 'task': 'task'})

    def test_update_object(self):
        '''
        Cannot change attributes of mocked AD objects.
        '''
        pass

    def test_remove_object(self):
        '''
        Cannot remove objects from mocked AD.
        '''
        pass
