import mock
import pytest

import simple_ad_manager as sadm


class TestArgumentParsing():
    '''
    Testing whether arguments and depenencies in the subparser are defined correctly. Ignoring the config validation
    and setting action to "None" to not trigger anything after the argparse.
    '''

    def setup_method(self):
        self.validate_config_patch = mock.patch('simple_ad_manager.cli.validate_config')
        self.validate_config = self.validate_config_patch.start()
        self.validate_config.return_value = None

        self.set_config_patch = mock.patch('simple_ad_manager.cli.set_config')
        self.set_config = self.set_config_patch.start()
        self.set_config.return_value = {'ldap-user': 'some.user',
                                        'ldap-pw': 'some.pw',
                                        'ldap-host': 'ldaps://ldaphost.test:636',
                                        'ldap-base': 'some.base'}

        self.set_action_patch = mock.patch('simple_ad_manager.cli.set_action')
        self.set_action = self.set_action_patch.start()
        self.set_action.return_value = {'action': None}

    def teardown_method(self):
        self.validate_config_patch.stop()
        self.set_action_patch.stop()
        self.set_config_patch.stop()

    def test_no_options(self):
        sadm.cli.main([])

    def test_has_verbose_short(self):
        sadm.cli.main(['-v'])

    def test_has_verbose_short_warning(self):
        sadm.cli.main(['-vvv'])

    def test_has_verbose_short_error(self):
        sadm.cli.main(['-vvvv'])

    def test_has_verbose_short_critical(self):
        sadm.cli.main(['-vvvvv'])

    def test_has_verbose_short_critical_and_over(self):
        sadm.cli.main(['-vvvvvvvvvvvvvvvvvvvvvvvvvvv'])

    def test_has_verbose_long(self):
        sadm.cli.main(['--verbose'])

    def test_has_ldap_user_arg(self):
        sadm.cli.main(['--ldap-user', 'username'])

    def test_has_ldap_pw_arg(self):
        sadm.cli.main(['--ldap-pw', 'password'])

    def test_has_ldap_host_arg(self):
        sadm.cli.main(['--ldap-host', 'some.hostname'])

    def test_has_list_action_no_object(self):
        sadm.cli.main(['list', ''])

    def test_has_list_action_with_object(self):
        sadm.cli.main(['list', 'some.object.type'])

    def test_has_list_action_with_object_and_base(self):
        sadm.cli.main(['list', '-s', 'some,base', 'some.object.type'])

    def test_has_list_action_with_object_base_and_r(self):
        sadm.cli.main(['list', '-s', 'some,base', '-r', 'some.object.type'])

    def test_has_create_group_action_blank(self):
        sadm.cli.main(['create', 'group', 'name'])

    def test_has_create_group_action_domain_local(self):
        sadm.cli.main(['create', 'group', 'name', '-dl'])

    def test_has_create_group_action_domain_global(self):
        sadm.cli.main(['create', 'group', 'name', '-dg'])

    def test_has_create_group_action_description(self):
        sadm.cli.main(['create', 'group', 'name', '-d', 'description'])

    def test_has_create_ou_action(self):
        sadm.cli.main(['create', 'ou', 'name'])

    def test_has_get_action(self):
        sadm.cli.main(['get', 'attribute', 'name'])

    def test_has_get_action_recursive(self):
        sadm.cli.main(['get', 'attribute', 'name', '-r'])

    def test_has_remove_action(self):
        sadm.cli.main(['remove', 'name'])

    def test_has_update_action_no_flag(self):
        sadm.cli.main(['update', 'name', 'attribute', 'value'])

    def test_has_update_action_r_flag(self):
        sadm.cli.main(['update', '-r', 'name', 'attribute', 'value'])

    def test_has_update_action_remove_flag(self):
        sadm.cli.main(['update', '--remove', 'name', 'attribute', 'value'])


class TestSetAction():
    '''
    Testing argument conversion to dict for conditional arguments.
    '''

    def setup_method(self):
        self.test_parser = sadm.cli.create_parser()

    def test_set_action_list_no_type_none_base(self):
        input_args = self.test_parser.parse_args(['list'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'all',
                           'searchbase': None,
                           'recursive': False,
                           'fulldn': False}

        assert(processed_values == expected_values)

    def test_set_action_list_with_type_none_base(self):
        input_args = self.test_parser.parse_args(['list', 'some.type'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'some.type',
                           'searchbase': None,
                           'recursive': False,
                           'fulldn': False}

        assert(processed_values == expected_values)

    def test_set_action_list_empty_search_base(self):
        input_args = self.test_parser.parse_args(['list', 'some.type', '-s', ''])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'some.type',
                           'searchbase': None,
                           'recursive': False,
                           'fulldn': False}

        assert(processed_values == expected_values)

    def test_set_action_list_with_search_base(self):
        input_args = self.test_parser.parse_args(['list', '-s', 'some,base', 'some.type'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'some.type',
                           'searchbase': 'some,base',
                           'recursive': False,
                           'fulldn': False}

        assert(processed_values == expected_values)

    def test_set_action_list_with_search_base_recursive(self):
        input_args = self.test_parser.parse_args(['list', 'some.type', '-s', 'some,base', '-r'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'some.type',
                           'searchbase': 'some,base',
                           'recursive': True,
                           'fulldn': False}

        assert(processed_values == expected_values)

    def test_set_action_list_with_search_base_recursive_fulldn(self):
        input_args = self.test_parser.parse_args(['list', 'some.type', '-s', 'some,base', '-r', '-f'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'list',
                           'type': 'some.type',
                           'searchbase': 'some,base',
                           'recursive': True,
                           'fulldn': True}

        assert(processed_values == expected_values)

    def test_set_action_create_group_blank(self):
        input_args = self.test_parser.parse_args(['create', 'group', 'some.group'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'create',
                           'type': 'group',
                           'name': 'some.group',
                           'domain': 'local',
                           'description': None}

        assert(processed_values == expected_values)

    def test_set_action_create_group_domain_local(self):
        input_args = self.test_parser.parse_args(['create', 'group', 'some.group', '-dl'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'create',
                           'type': 'group',
                           'name': 'some.group',
                           'domain': 'local',
                           'description': None}

        assert(processed_values == expected_values)

    def test_set_action_create_group_domain_global_empty_descr(self):
        input_args = self.test_parser.parse_args(['create', 'group', 'some.group', '-dg', '-d', ''])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'create',
                           'type': 'group',
                           'name': 'some.group',
                           'domain': 'global',
                           'description': None}

        assert(processed_values == expected_values)

    def test_set_action_create_group_domain_global_with_descr(self):
        input_args = self.test_parser.parse_args(['create', 'group', 'some.group', '-dg', '-d', 'some description'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'create',
                           'type': 'group',
                           'name': 'some.group',
                           'domain': 'global',
                           'description': 'some description'}

        assert(processed_values == expected_values)

    def test_set_action_create_ou(self):
        input_args = self.test_parser.parse_args(['create', 'ou', 'some.ou'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'create',
                           'type': 'ou',
                           'name': 'some.ou',
                           'domain': 'local',
                           'description': None}

        assert(processed_values == expected_values)

    def test_set_action_get(self):
        input_args = self.test_parser.parse_args(['get', 'some.attribute', 'some.object'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'get',
                           'attribute': 'some.attribute',
                           'name': 'some.object',
                           'recursive': False}

        assert(processed_values == expected_values)

    def test_set_action_get_none_member_recursive(self):
        input_args = self.test_parser.parse_args(['get', 'some.attribute', 'some.object', '-r'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'get',
                           'attribute': 'some.attribute',
                           'name': 'some.object',
                           'recursive': True}

        assert(processed_values == expected_values)

    def test_set_action_remove(self):
        input_args = self.test_parser.parse_args(['remove', 'some.name'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'remove',
                           'name': 'some.name'}

        assert(processed_values == expected_values)

    def test_set_action_update_no_flag(self):
        input_args = self.test_parser.parse_args(['update', 'some.name', 'some.attribute', 'some.value'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'update',
                           'name': 'some.name',
                           'task': 'add',
                           'attribute': 'some.attribute',
                           'value': 'some.value'}

        assert(processed_values == expected_values)

    def test_set_action_update_remove_flag(self):
        input_args = self.test_parser.parse_args(['update', '-r', 'some.name', 'some.attribute', 'some.value'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'update',
                           'name': 'some.name',
                           'task': 'remove',
                           'attribute': 'some.attribute',
                           'value': 'some.value'}

        assert(processed_values == expected_values)

    def test_set_action_update_update_flag(self):
        input_args = self.test_parser.parse_args(['update', '-u', 'some.name', 'some.attribute', 'some.value'])
        processed_values = sadm.cli.set_action(input_args)
        expected_values = {'action': 'update',
                           'name': 'some.name',
                           'task': 'update',
                           'attribute': 'some.attribute',
                           'value': 'some.value'}

        assert(processed_values == expected_values)


class TestCfg():

    def setup_method(self):
        self.test_parser = sadm.cli.create_parser()

        self.set_action_patch = mock.patch('simple_ad_manager.cli.set_action')
        self.set_action = self.set_action_patch.start()
        self.set_action.return_value = {'action': None}

        self.envvars = {'ldap-user': 'env.user',
                        'ldap-pw': 'env.pw',
                        'ldap-host': 'env.host',
                        'ldap-base': 'env.base'}

        self.cfgfilevars = {'ldap-user': 'cfg.user',
                            'ldap-pw': 'cfg.pw',
                            'ldap-host': 'cfg.host',
                            'ldap-base': 'cfg.base'}

        self.cliargs = ['--ldap-user', 'arg.user',
                        '--ldap-pw', 'arg.pw',
                        '--ldap-host', 'arg.host',
                        '--ldap-base', 'arg.base']

    def teardown_method(self):
        self.set_action_patch.stop()

    def test_validate_config_no_ldap_user_set(self):
        return_value = {}
        with pytest.raises(sadm.err.MissingLdapUser):
            assert sadm.cli.validate_config(return_value)

    def test_no_ldap_pw_set(self):
        return_value = {'ldap-user': 'some.user'}
        with pytest.raises(sadm.err.MissingLdapPW):
            assert sadm.cli.validate_config(return_value)

    def test_no_ldap_host_set(self):
        return_value = {'ldap-user': 'some.user', 'ldap-pw': 'some.pw'}
        with pytest.raises(sadm.err.MissingLdapHost):
            assert sadm.cli.validate_config(return_value)

    def test_set_config_no_cli_args_no_config_file(self):
        '''
        If no args are set or config file found, the envvars need to be kept.
        '''
        with mock.patch('simple_ad_manager.cli.get_envvars', return_value=self.envvars):
            with mock.patch('simple_ad_manager.cli.check_config_file', return_value={}):
                assert(sadm.cli.set_config(self.test_parser.parse_args([])) == self.envvars)

    def test_set_config_config_file_only(self):
        '''
        If only the config file is given, its parameters are to be used.
        '''
        expectedvars = {'ldap-user': 'cfg.user',
                        'ldap-pw': 'cfg.pw',
                        'ldap-host': 'cfg.host',
                        'ldap-base': 'cfg.base'}
        with mock.patch('simple_ad_manager.cli.get_envvars', return_value={}):
            with mock.patch('simple_ad_manager.cli.check_config_file', return_value=self.cfgfilevars):
                testargs = self.test_parser.parse_args([])
                assert(sadm.cli.set_config(testargs) == expectedvars)

    def test_set_config_all_ldap_args_given_with_config_file(self):
        '''
        If a config file is given but the cli args set, then only the cli args are to be considered.
        '''
        expectedvars = {'ldap-user': 'arg.user',
                        'ldap-pw': 'arg.pw',
                        'ldap-host': 'arg.host',
                        'ldap-base': 'arg.base'}
        with mock.patch('simple_ad_manager.cli.get_envvars', return_value=self.envvars):
            with mock.patch('simple_ad_manager.cli.check_config_file', return_value=self.cfgfilevars):
                testargs = self.test_parser.parse_args(self.cliargs)
                assert(sadm.cli.set_config(testargs) == expectedvars)

    def test_get_envvars_syntax_only(self):
        sadm.cli.get_envvars()

    def test_chech_config_file_syntax_only(self):
        sadm.cli.check_config_file()

    def test_establish_ldap_connection(self):
        '''
        Not testable without actual LDAP connection available.
        '''
        pass
