class Error(Exception):
    pass


class ConfigFileError(Error):
    pass


class LdapError(Error):
    pass


class LdapBindFail(Error):
    pass


class LdapProcessError(Error):
    pass


class LdapSearchError(Error):
    pass


class MissingLdapUser(Error):
    pass


class MissingLdapPW(Error):
    pass


class MissingLdapHost(Error):
    pass


class UnsupportedArgument(Error):
    pass
