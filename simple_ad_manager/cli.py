from __future__ import absolute_import

import argparse
import json
import logging
import os

from ldap3 import Server, Connection, ALL

from . import err
from . import sadm

log = logging.getLogger(__name__)


def get_envvars(env=None):
    if env is None:
        env = os.environ
        envvars = {}

    ldap_user = env.get('LDAP_USER')
    if ldap_user is not None:
        envvars['ldap-user'] = ldap_user

    ldap_pw = env.get('LDAP_PW')
    if ldap_pw is not None:
        envvars['ldap-pw'] = ldap_pw

    ldap_host = env.get('LDAP_HOST')
    if ldap_host is not None:
        envvars['ldap-host'] = ldap_host

    ldap_base = env.get('LDAP_BASE')
    if ldap_base is not None:
        envvars['ldap-base'] = ldap_base

    return envvars


def check_config_file():
    userhome = os.path.expanduser('~')
    cfile = userhome + '/.ldaprc'
    config = {}

    try:
        with open(cfile) as config_json:
            config = json.load(config_json)
    except FileNotFoundError:
        log.info('{cfile} not found.'.format(cfile=cfile))
    except json.decoder.JSONDecodeError:
        log.warning('{cfile} not formatted in json.'.format(cfile=cfile))

    return config


def set_config(args):
    cfile = check_config_file()
    if cfile != {}:
        cfg = cfile

    else:
        cfg = get_envvars()

    if args.ldap_user:
        cfg['ldap-user'] = args.ldap_user
    if args.ldap_pw:
        cfg['ldap-pw'] = args.ldap_pw
    if args.ldap_host:
        cfg['ldap-host'] = args.ldap_host
    if args.ldap_base:
        cfg['ldap-base'] = args.ldap_base

    return cfg


def validate_config(cfg):

    if cfg.get('ldap-user') is None:
        msg = 'LDAP user must not be empty.'
        log.error(msg)
        raise err.MissingLdapUser()

    if cfg.get('ldap-pw') is None:
        msg = 'LDAP password must not be empty.'
        log.error(msg)
        raise err.MissingLdapPW()

    if cfg.get('ldap-host') is None:
        msg = 'LDAP host must not be empty.'
        log.error(msg)
        raise err.MissingLdapHost()


def create_parser():
    parser = argparse.ArgumentParser(description='A command line interface for LDAP')

    # Verbosity settings
    parser.add_argument(
        '-v', '--verbose',
        action='count', dest='verbose', default=False,
        help='Increase verbosity.',
    )

    # LDAP settings - ideally set as config file or env vars
    parser.add_argument(
        '--ldap-user',
        help='LDAP user with required permissions on target AD. Full path required: CN=user,OU=someOU,DC=dc,DC=com',
        metavar='LDAP_USER',
    )
    parser.add_argument(
        '--ldap-pw',
        help='LDAP password of LDAP user.',
        metavar='LDAP_PW',
    )
    parser.add_argument(
        '--ldap-host',
        help='LDAP protocoll host and port to connect to: ldaps://ldap.host:636',
        metavar='LDAP_HOST',
    )
    parser.add_argument(
        '--ldap-base',
        help='LDAP search base: DC=dc,DC=com',
        metavar='LDAP_BASE',
    )

    # create a subparser so required parameters depend on called function
    commands = parser.add_subparsers(dest='action')

    # list parameters
    list_object = commands.add_parser('list', help='List objects within a given search base')
    list_object.add_argument(
        '-r', '--recursive',
        action='store_true',
        help='Search through all child OUs',
    )
    list_object.add_argument(
        '-s', '--searchbase',
        action='store',
        help='Optional, defaults to ldap-base. "level2,level1" will result in "OU=level2,OU=level1"+ldap-base',
        nargs='?',
    )
    list_object.add_argument(
        'objectType',
        action='store',
        help='AD objectType ("OU", "user", "group", default=%(default)s)',
        default='all',
        nargs='?',
    )
    list_object.add_argument(
        '-f', '--fulldn',
        action='store_true',
        help='Return object names as full distinguishedName.',
    )

    create_object = commands.add_parser('create', help='Create AD objects')
    create_object.add_argument(
        'type',
        action='store',
        help='Type of object to create',
    )
    create_object.add_argument(
        'name',
        action='store',
        help='full DN: CN=MY_GROUP,OU=SOME_OU,DC=example,DC=com',
    )
    domain = create_object.add_mutually_exclusive_group()
    domain.add_argument(
        '-dl',
        action='store_true',
        help='Create a domain local group'
    )
    domain.add_argument(
        '-dg',
        action='store_true',
        help='Create a domain global group'
    )
    create_object.add_argument(
        '-d', '--description',
        action='store',
        help='Describe purpose of created object',
    )

    get_attribute = commands.add_parser('get', help='Get AD object attribute')
    get_attribute.add_argument(
        'attribute',
        action='store',
        help='AD object attribute, e.g. desciption, member or memberof',
    )
    get_attribute.add_argument(
        'name',
        action='store',
        help='CN of object or full DN',
    )
    get_attribute.add_argument(
        '-r', '--recursive',
        action='store_true',
        help='Make search recursive.',
    )

    remove_object = commands.add_parser('remove', help='Remove AD object')
    remove_object.add_argument(
        'name',
        action='store',
        help='AD object to delete',
    )

    update_object = commands.add_parser('update', help='Update AD object attribute (default = add)')
    update_object.add_argument(
        'name',
        action='store',
        help='CN of object or full DN',
    )
    update_action = update_object.add_mutually_exclusive_group()
    update_action.add_argument(
        '-u', '--update',
        action='store_true',
        help='update attribute value',
    )
    update_action.add_argument(
        '-r', '--remove',
        action='store_true',
        help='remove attribute value',
    )
    update_object.add_argument(
        'attribute',
        action='store',
        help='AD attribute to update, e.g. description, member',
    )
    update_object.add_argument(
        'value',
        action='store',
        help='can be "all" to target all attribute values',
    )

    return parser


def set_action(args):
    if vars(args)['action']:
        action = {'action': vars(args)['action']}

        if vars(args)['action'] == 'list':
            action['type'] = vars(args)['objectType']
            action['searchbase'] = vars(args)['searchbase']
            action['recursive'] = vars(args)['recursive']
            action['fulldn'] = vars(args)['fulldn']
            if vars(args)['searchbase'] == '':
                action['searchbase'] = None

        if vars(args)['action'] == 'create':
            # Defaulting to domain local!
            action['type'] = vars(args)['type']
            action['name'] = vars(args)['name']
            action['domain'] = 'local'
            action['description'] = vars(args)['description']
            if vars(args)['dl']:
                action['domain'] = 'local'
            if vars(args)['dg']:
                action['domain'] = 'global'
            if vars(args)['description'] == '':
                action['description'] = None

        if vars(args)['action'] == 'get':
            # Recursive flag intended for group members only
            if vars(args)['recursive']:
                if vars(args)['attribute'] != 'member':
                    msg = 'Ignoring recursive flag. Only supported for member attribute.'
                    log.info(msg)
            action['attribute'] = vars(args)['attribute']
            action['name'] = vars(args)['name']
            action['recursive'] = vars(args)['recursive']

        if vars(args)['action'] == 'remove':
            action['name'] = vars(args)['name']

        if vars(args)['action'] == 'update':
            action['name'] = vars(args)['name']
            action['task'] = 'add'
            action['attribute'] = vars(args)['attribute']
            action['value'] = vars(args)['value']
            if vars(args)['update']:
                action['task'] = 'update'
            if vars(args)['remove']:
                action['task'] = 'remove'

    else:
        log.error('Action could not be set from arguments: {args}'.format(args=args))

    return action


def establish_ldap_connection(cfg):
    ldap_srv = Server(cfg['ldap-host'], get_info=ALL)
    ldap_con = Connection(ldap_srv, cfg['ldap-user'], password=cfg['ldap-pw'])

    if not ldap_con.bind():
        msg = 'LDAP bind failed: {res}'.format(res=ldap_con.result)
        log.error(msg)
        raise err.LdapBindFail(msg)

    return ldap_con


def main(argv=None, namespace=None):
    # get arguments
    parser = create_parser()
    args = parser.parse_args(argv, namespace)

    # set logging config
    loglevel = logging.INFO
    if args.verbose:
        verbosity = args.verbose * 10
    else:
        verbosity = 20
    if verbosity == 10:
        loglevel = logging.DEBUG
    if verbosity == 20:
        loglevel = logging.INFO
    if verbosity == 30:
        loglevel = logging.WARNING
    if verbosity == 40:
        loglevel = logging.ERROR
    if verbosity >= 50:
        loglevel = logging.CRITICAL

    logging.basicConfig(level=loglevel)

    # set LDAP config parameters and validate them
    cfg = set_config(args)
    validate_config(cfg)

    # trigger simple_ad_manager module based on given action
    action = set_action(args)

    if action['action'] == 'list':
        con = establish_ldap_connection(cfg)
        dc = sadm.DomainController(cfg, con)
        for entry in dc.list_object(action):
            print(entry)

    if action['action'] == 'create':
        con = establish_ldap_connection(cfg)
        dc = sadm.DomainController(cfg, con)
        dc.create_object(action)

    if action['action'] == 'get':
        con = establish_ldap_connection(cfg)
        dc = sadm.DomainController(cfg, con)
        for entry in dc.get_attribute(action):
            print(entry)

    if action['action'] == 'remove':
        con = establish_ldap_connection(cfg)
        dc = sadm.DomainController(cfg, con)
        dc.remove_object(action)

    if action['action'] == 'update':
        con = establish_ldap_connection(cfg)
        dc = sadm.DomainController(cfg, con)
        dc.update_object(action)
