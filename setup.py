from setuptools import setup

setup(
    name='Simple AD Manager',
    version='2.1.2',
    description='CLI to simplify basic Active Directory operations',
    author='Martin Olschewski',
    packages=['simple_ad_manager'],
    install_requires=['ldap3==2.6'],
    setup_requires=['pytest-runner'],
    tests_require=[
        'mock',
        'pytest',
        'pytest-cov',
        'pytest-flake8',
    ],
    scripts=['sadm'],
    license='MIT',
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
    ],
)
