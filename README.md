# Simple AD Manager
It is a Python3 LDAP command line interface to control a Microsoft Active Directory. Alternatively the "sadm" module simplifies performing AD tasks from within your own scripts.
# LDAP Configuration
This tool can be configured through environment variables, a config file or command line arguments. The latter will override the former.
## Environment variables 
* LDAP_USER='CN=Firstname Lastname,OU=Users,DC=example,DC=com'
* LDAP_PW='69strongsecret420'
* LDAP_HOST='ldaps://ldap.host:636'
' LDAP_BASE='DC=example,DC=com'
## Config file
Copy the .ldaprc to your home folder and adjust credentials.
